
import ex04.ConsoleCommand;
import ex04.Menu;
import ex07.Item;
import ex07.Items;
import ex07.ItemsGenerator;
import ex07.ItemsSorter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mezinn
 */
public class main {

    abstract class ConsoleCmd implements ConsoleCommand {

        /**
         * Коллекция объектов {@linkplain Items}
         */
        protected Items items;
        /**
         * Отображаемое название команды
         */
        private String name;
        /**
         * Символ горячей клавиши команды
         */
        private char key;

        /**
         * Инициализирует поля консольной команды
         *
         * @param items {@linkplain ConsoleCmd#items}
         * @param name {@linkplain ConsoleCmd#name}
         * @param key {@linkplain ConsoleCmd#key}
         */
        public ConsoleCmd(Items items, String name, char key) {
            this.items = items;
            this.name = name;
            this.key = key;
        }

        public char getKey() {
            return key;
        }

        public String toString() {
            return name;
        }
    }

    /**
     * Устанавливает связь наблюдателей с наблюдаемыми объектами; реализует
     * диалог с пользователем
     */
    public void run() {
        Items items = new Items();
        ItemsGenerator generator = new ItemsGenerator();
        ItemsSorter sorter = new ItemsSorter();
        items.addObserver(generator);
        items.addObserver(sorter);
        Menu menu = new Menu();
        menu.add(new ConsoleCmd(items, "'v'iew", 'v') {

            public void exec() {

                System.out.println(items.getItems());
            }
        });
        menu.add(new ConsoleCmd(items, "'a'dd", 'a') {
            public void exec() {
                items.add(new Item(Math.random() * 360, Math.random() * 360));
            }
        });
        menu.add(new ConsoleCmd(items, "'d'el", 'd') {

            public void exec() {
                items.del((int) Math.round(Math.random() * (items.getItems().size() - 1)));
            }
        });
        menu.exec();
    }

    /**
     * Выполняется при запуске программы
     *
     * @param args параметры запуска программы
     */
    public static void main(String[] args) {
        new main().run();;
    }
}
