/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex07;

/**
 *
 * @author mezinn
 */
public class ItemsGenerator extends AnnotatedObserver {

    @Event(Items.ITEMS_EMPTY)
    public void itemsEmpty(Items observable) {
        for (Item item : observable) {
            item.setH(Math.random() * 360);
            item.setM(Math.random() * 360);
        }
        observable.call(Items.ITEMS_CHANGED);
    }
}
