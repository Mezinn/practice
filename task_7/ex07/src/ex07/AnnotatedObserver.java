/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex07;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author mezinn
 */
public class AnnotatedObserver implements Observer {

    private Map<Object, Method> handlers = new HashMap<Object, Method>();

    public AnnotatedObserver() {
        for (Method m : this.getClass().getMethods()) {
            if (m.isAnnotationPresent(Event.class)) {
                handlers.put(m.getAnnotation(Event.class).value(), m);
            }
        }
    }

    public void handleEvent(Observable observable, Object event) {
        Method m = handlers.get(event);
        try {
            if (m != null) {
                m.invoke(this, observable);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}
