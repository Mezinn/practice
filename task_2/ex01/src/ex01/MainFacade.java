/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex01;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author mezinn
 */
public class MainFacade {

    private ItemSerializer item_serializer = new ItemSerializer(new Item(0, 0));

    /*
    * Функція для виводу меню (обгортка). 
     */
    public void run() throws Exception {
        this.menu();
    }

    /*
    *  Функція для виводу меню на екран
     */
    private void menu() throws Exception {
        String input_string = null;
        BufferedReader buffer_reader = new BufferedReader(new InputStreamReader(System.in));
        do {
            do {
                System.out.println("Enter command...");
                System.out.print("'q'uit, 'v'iew, 'g'enerate, 's'ave, 'r'estore: ");
                try {
                    input_string = buffer_reader.readLine();
                } catch (IOException e) {
                    System.out.println("Error: " + e);
                    System.exit(0);
                }
            } while (input_string.length() != 1);
            switch (input_string.charAt(0)) {
                case 'q':
                    System.out.println("Exit.");
                    break;
                case 'v':
                    System.out.println("View current.");
                    System.out.println(this.item_serializer.getItem());
                    break;
                case 'g':
                    System.out.println("Random generation.");
                    this.item_serializer = new ItemSerializer(new Item(Math.random() * 360.0, Math.random() * 360.0));
                    System.out.println(this.item_serializer.getItem());
                    break;
                case 's':
                    System.out.println("Save current.");
                    try {
                        this.item_serializer.serialize();
                    } catch (IOException e) {
                        System.out.println("Serialization error: " + e);
                    }
                    System.out.println(this.item_serializer.getItem());
                    break;
                case 'r':
                    System.out.println("Restore last saved.");
                    try {
                        this.item_serializer.unserialize();
                    } catch (Exception e) {
                        System.out.println("Serialization error: " + e);
                    }
                    System.out.println(this.item_serializer.getItem());
                    break;
                default:
                    System.out.print("Wrong command. ");
            }
        } while (input_string.charAt(0) != 'q');
    }
}
