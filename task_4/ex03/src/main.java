
import ex02.FormattedMainFacade;
import ex02.ItemSerializer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mezinn
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        FormattedMainFacade main_facade = new FormattedMainFacade();
        main_facade.run();
    }
    
}
