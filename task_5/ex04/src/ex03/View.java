/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex03;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author mezinn
 */
public class View {

    private static final String serialized_file_name = "serialize_data.bin";

    private ArrayList<Item> items = new ArrayList<>();

    public void view_show() {
        for (Item item : this.items) {
            System.out.println(item);
        }
    }

    public void view_init() {
        for (int i = 0; i < Math.floor(Math.random() * 360.0); i++) {
            this.items.add(new Item(Math.random() * 360, Math.random() * 360));
        }
    }

    public void restore() {
        try (ObjectInputStream object_input_stream = new ObjectInputStream(new FileInputStream(serialized_file_name))) {
            this.items = (ArrayList<Item>) object_input_stream.readObject();
            object_input_stream.close();
        } catch (Exception ex) {
            System.out.println("Serialized error.");
        }
    }

    public void save() {
        try (ObjectOutputStream object_output_stream = new ObjectOutputStream(new FileOutputStream(serialized_file_name))) {
            object_output_stream.writeObject(this.items);
            object_output_stream.flush();
            object_output_stream.close();
        } catch (Exception ex) {
            System.out.println("Serialized error.");
        }
    }

    public Iterable<Item> get_items() {
        return this.items;
    }

}
