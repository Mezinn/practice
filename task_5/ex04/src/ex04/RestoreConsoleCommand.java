/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex04;
import ex03.View;
public class RestoreConsoleCommand implements ConsoleCommand {
    private View view;
    public RestoreConsoleCommand(View view) {
        this.view = view;
    }
    public char getKey() {
        return 'r';
    }
    public String toString() {
        return "'r'estore";
    }
    public void exec() {
        try {
            this.view.restore();
        } catch (Exception e) {
            System.err.println("Serialization error: " + e);
        }
        this.view.view_show();
    }
}
