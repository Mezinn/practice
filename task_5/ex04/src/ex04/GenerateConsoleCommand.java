/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex04;
import ex03.View;
public class GenerateConsoleCommand implements ConsoleCommand {
    private View view;
    public GenerateConsoleCommand(View view) {
        this.view = view;
    }
    public char getKey() {
        return 'g';
    }
    public String toString() {
        return "'g'enerate";
    }
    public void exec() {
        System.out.println("Random generation.");
        this.view.view_init();
        this.view.view_show();
    }
}
