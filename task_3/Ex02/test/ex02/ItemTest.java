/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex02;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mezinn
 */
public class ItemTest {

    public ItemTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getM method, of class Item.
     */
    @Test
    public void testGetM() {
        System.out.println("getM");
        Item instance = new Item(0, 0);
        double expResult = 0.0;
        double result = instance.getM();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getH method, of class Item.
     */
    @Test
    public void testGetH() {
        System.out.println("getH");
        Item instance = new Item(0, 0);
        double expResult = 0.0;
        double result = instance.getH();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getValue method, of class Item.
     */
    @Test
    public void testGetValue() {
        System.out.println("getValue");
        Item instance = new Item(0, 0);
        double expResult = 0.0;
        double result = instance.getValue();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getValue_binary method, of class Item.
     */
    @Test
    public void testGetValue_binary() {
        System.out.println("getValue_binary");
        Item instance = new Item(0, 0);
        String expResult = "0";
        String result = instance.getValue_binary();
        assertEquals(expResult, result);
    }

    /**
     * Test of getMax_count method, of class Item.
     */
    @Test
    public void testGetMax_count() {
        System.out.println("getMax_count");
        Item instance = new Item(0, 0);
        int expResult = 0;
        int result = instance.getMax_count();
        assertEquals(expResult, result);
    }

    /**
     * Test of run_longest_index method, of class Item.
     */
    @Test
    public void testRun_longest_index() {
        System.out.println("run_longest_index");
        String setofletters = "";
        int expResult = 0;
        int result = Item.run_longest_index(setofletters);
        assertEquals(expResult, result);
    }

}
