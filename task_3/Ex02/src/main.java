
import ex02.MainFacade;
/**
 *
 * @author mezinn
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        MainFacade main_facade = new MainFacade();
        main_facade.run();
    }

}
