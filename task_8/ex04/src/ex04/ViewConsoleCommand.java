/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex04;
import ex03.View;
public class ViewConsoleCommand implements ConsoleCommand {
    private View view;
    public ViewConsoleCommand(View view) {
        this.view = view;
    }
    public char getKey() {
        return 'v';
    }
    public String toString() {
        return "'v'iew";
    }
    public void exec() {
        System.out.println("View current.");
        this.view.view_show();
    }
}
