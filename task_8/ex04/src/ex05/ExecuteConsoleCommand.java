/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex05;

import ex03.View;
import ex04.ConsoleCommand;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author mezinn
 */
public class ExecuteConsoleCommand implements ConsoleCommand {

    private View view;

    public View getView() {
        return view;
    }

    public View setView(View view) {
        return this.view = view;
    }

    public ExecuteConsoleCommand(View view) {
        this.view = view;
    }

    public char getKey() {
        return 'e';
    }

    @Override
    public String toString() {
        return "'e'xecute";
    }

    public void exec() {
        /**/
        CommandQueue queue1 = new CommandQueue();
        CommandQueue queue2 = new CommandQueue();
        /**
         * ExecutorService exec1 = Executors.newSingleThreadExecutor();
         * ExecutorService exec2 = Executors.newSingleThreadExecutor(); /*
         */
        MaxCommand maxCommand = new MaxCommand((View) view);
        AvgCommand avgCommand = new AvgCommand((View) view);
        MinMaxCommand minMaxCommand = new MinMaxCommand((View) view);
        System.out.println("Execute all threads...");
        /**
         * exec1.execute(minMaxCommand); exec2.execute(maxCommand);
         * exec2.execute(avgCommand); /*
         */
        queue1.put(minMaxCommand);
        queue2.put(maxCommand);
        queue2.put(avgCommand);
        /**/
        try {
            while (avgCommand.running()
                    || maxCommand.running()
                    || minMaxCommand.running()) {
                TimeUnit.MILLISECONDS.sleep(100);
            }
            /**
             * exec1.shutdown(); exec2.shutdown(); /*
             */
            queue1.shutdown();
            queue2.shutdown();
            /**/
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            System.err.println(e);
        }
        System.out.println("All done.");
    }

}
