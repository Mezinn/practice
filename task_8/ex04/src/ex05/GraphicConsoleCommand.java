/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ex05;

import ex03.View;
import ex04.ConsoleCommand;

/**
 *
 * @author mezinn
 */
public class GraphicConsoleCommand implements ConsoleCommand {

    private View view;

    public GraphicConsoleCommand(View view) {
        this.view = view;
    }

    public char getKey() {
        return 's';
    }

    public void exec() {
        Runnable thread = new GraphicThread(this.view);
        new Thread(thread).start();
    }

    public String toString() {
        return "'s'how graphic";
    }

}
