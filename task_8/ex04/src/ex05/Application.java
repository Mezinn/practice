package ex05;

import ex04.*;
import ex03.View;

public class Application {

    private static Application instance = new Application();

    public Application() {
    }

    public static Application getInstance() {
        return instance;
    }
    private View view = new View();
    private Menu menu = new Menu();

    public void run() {
        menu.add(new ViewConsoleCommand(view));
        menu.add(new GraphicConsoleCommand(view));
        menu.add(new GenerateConsoleCommand(view));
        menu.add(new ChangeConsoleCommand(view));
        menu.add(new ExecuteConsoleCommand(view));
        menu.exec();
    }
}
